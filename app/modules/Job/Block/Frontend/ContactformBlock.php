<?php
namespace App\Module\Job\Block\Frontend;

use App\System\Block;

class ContactformBlock extends Block {

    protected $_template = "job/contact-form";

}