<?php
namespace App\Module\Page\Block\Frontend;

use App\System\Block;

class TimelineBlock extends Block {

    protected $_template = "page/timeline";

    protected $_options = [];

}