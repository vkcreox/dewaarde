<?php
namespace App\Module\Core\Block\Frontend;

use App\System\Block;

class ContactformBlock extends Block {

    protected $_template = "core/contact-form";

}