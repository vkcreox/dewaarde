<?php
namespace App\Module\Core\Block\Frontend;

use App\System\Block;

class ContactBlock extends Block {

    protected $_template = "core/contact";

}