<?php
namespace App\Module\Core\Block\Frontend;

use App\System\Block;

class FooterBlock extends Block {

    protected $_template = "core/footer";

}