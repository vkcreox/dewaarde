<?php
namespace App\Module\Core\Block\Admin;

use App\System\App;
use App\System\Block;

class MenuBlock extends Block {

    protected $_template = "core/menu";

}