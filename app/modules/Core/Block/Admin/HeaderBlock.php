<?php
namespace App\Module\Core\Block\Admin;

use App\System\App;
use App\System\Block;

class HeaderBlock extends Block {

    protected $_template = "core/header";

}