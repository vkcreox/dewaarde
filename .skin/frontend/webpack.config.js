const ExtractTextPlugin = require("extract-text-webpack-plugin");
const webpack = require('webpack');

module.exports = {
  entry: {main: './src/js/app.js'},
  output: {
    path: __dirname + '/../../www/skin/frontend',
    filename: 'bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: ['css-loader', 'sass-loader']
        })
      },
      {
        test:
          require.resolve('jquery'),
        use:
          [{
            loader: 'expose-loader',
            options: '$'
          },{
            loader: 'expose-loader',
            options: 'jQuery'
          }]
      }]
  },
  plugins:
    [
      new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
      }),
      new ExtractTextPlugin({filename: 'style.css'})
    ],
  resolve:
    {
      alias: {
        vue: 'vue/dist/vue.js'
      }
    }
}
